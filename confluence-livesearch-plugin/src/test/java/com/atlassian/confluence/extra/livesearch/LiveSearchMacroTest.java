package com.atlassian.confluence.extra.livesearch;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.renderer.v2.macro.MacroException;
import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LiveSearchMacroTest
{
    public static final String SPACE_NAME = "space name";
    public static final String PAGE_EXCERPT = "page excerpt";
    @Mock private SpaceManager spaceManager;
    @Mock private VelocityHelperService velocityHelperService;
    private Map<String, Object> macroParameters;
    private LiveSearchMacro liveSearchMacro;

    @Before
    public void setUp() throws Exception
    {
        macroParameters = new HashMap<String, Object>();
        liveSearchMacro = new LiveSearchMacro(spaceManager, velocityHelperService);
        when(velocityHelperService.createDefaultVelocityContext()).thenReturn(new HashMap<String, Object>());
    }

    @After
    public void tearDown() throws Exception
    {
        spaceManager = null;
        velocityHelperService = null;
    }

    @Test
    public void testSpaceResolvedWhenValidSpaceKeySpecified() throws MacroException
    {
        final Space space = new Space("TST");
        space.setName("Test Space");

        macroParameters.put("spaceKey", space.getKey());
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);
        liveSearchMacro.execute(macroParameters, StringUtils.EMPTY, new Page().toPageContext());
        verify(velocityHelperService).getRenderedTemplate(Matchers.anyString(), Matchers.argThat(new BaseMatcher<Map<String, Object>>()
        {
            @SuppressWarnings("unchecked")
            public boolean matches(Object o)
            {
                Map<String, Object> map = (Map<String, Object>) o;
                return map.get("where").equals(space.getKey());
            }

            public void describeTo(Description description)
            {
                description.appendText("Velocity context that contains both the space key and space name");
            }
        }));
    }

    @Test
    public void testSpaceNotResolvedWhenInvalidSpaceKeySpecified() throws MacroException
    {
        macroParameters.put("spaceKey", "TST");

        liveSearchMacro.execute(macroParameters, StringUtils.EMPTY, new Page().toPageContext());

        verify(velocityHelperService).getRenderedTemplate(Matchers.anyString(), Matchers.argThat(new BaseMatcher<Map<String, Object>>()
        {
            @SuppressWarnings("unchecked")
            public boolean matches(Object o)
            {
                Map<String, Object> map = (Map<String, Object>) o;
                return !map.containsKey("spaceKey");
            }

            public void describeTo(Description description)
            {
                description.appendText("A Velocity context that does not contain a spaceKey");
            }
        }));
    }

    @Test
    public void testAdditionalDefaultToSpaceName() throws MacroException
    {
        macroParameters.put("spaceKey", "TST");

        liveSearchMacro.execute(macroParameters, StringUtils.EMPTY, new Page().toPageContext());

        verify(velocityHelperService).getRenderedTemplate(Matchers.anyString(), Matchers.argThat(new BaseMatcher<Map<String, Object>>()
        {
            @SuppressWarnings("unchecked")
            public boolean matches(Object o)
            {
                Map<String, Object> map = (Map<String, Object>) o;
                return map.get("additional").equals(SPACE_NAME);
            }

            public void describeTo(Description description)
            {
                description.appendText("A Velocity context that contains an additional param equal to 'space name'");
            }
        }));
    }

    @Test
    public void testAdditionalOverrideDefault() throws MacroException
    {
        macroParameters.put("spaceKey", "TST");
        macroParameters.put("additional", PAGE_EXCERPT);

        liveSearchMacro.execute(macroParameters, StringUtils.EMPTY, new Page().toPageContext());

        verify(velocityHelperService).getRenderedTemplate(Matchers.anyString(), Matchers.argThat(new BaseMatcher<Map<String, Object>>()
        {
            @SuppressWarnings("unchecked")
            public boolean matches(Object o)
            {
                Map<String, Object> map = (Map<String, Object>) o;
                return map.get("additional").equals(macroParameters.get("additional"));
            }

            public void describeTo(Description description)
            {
                description.appendText("A Velocity context that contains an additional param equal to context provided");
            }
        }));
    }
}
