package it.com.atlassian.confluence.plugins.livesearch;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

public class TestContext
{
    final List<Page> pages;
    final List<Space> spaces;
    final ConfluenceRpc rpc;

    private TestContext(Builder builder) {
        this.rpc = builder.rpc;
        this.pages = Collections.unmodifiableList(builder.pages);
        this.spaces = Collections.unmodifiableList(builder.spaces);
    }

    public static Builder builder() {
        return new Builder();
    }

    public void create()
    {
        for (Space space : spaces) rpc.createSpace(space);
        for (Page page : pages) rpc.createPage(page);
    }

    public void delete()
    {
        for (Page page : pages) rpc.removePage(page);
        for (Space space : spaces) rpc.removeSpace(space.getKey());
    }

    public static class Builder
    {
        private final List<Space> spaces = Lists.newArrayList();
        private final List<Page> pages = Lists.newArrayList();
        private ConfluenceRpc rpc;

        public Builder setRpc(ConfluenceRpc rpc)
        {
            this.rpc = rpc;
            return this;
        }

        public Builder addPages(Page...  pagesToAdd)
        {
            Collections.addAll(pages, pagesToAdd);
            return this;
        }

        public Builder addSpaces(Space... spacesToAdd)
        {
            Collections.addAll(spaces, spacesToAdd);
            return this;
        }

        public TestContext build()
        {
            return new TestContext(this);
        }
    }
}
