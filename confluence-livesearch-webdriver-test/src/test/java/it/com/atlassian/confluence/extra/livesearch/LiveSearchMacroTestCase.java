package it.com.atlassian.confluence.extra.livesearch;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.pageobjects.page.search.SearchPage;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import it.com.atlassian.confluence.plugins.livesearch.TestContext;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchMacro;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchSearchResult;
import org.junit.After;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static it.com.atlassian.confluence.plugins.livesearch.LiveSearchMacroTestUtil.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class LiveSearchMacroTestCase extends AbstractWebDriverTest
{

    @After
    public void tearDown()
    {
        rpc.removeSpace(Space.TEST.getKey());
    }

    private LiveSearchMacro getLiveSearchMacro(Page page)
    {
        rpc.logIn(User.CONF_ADMIN);
        product.loginAndView(User.TEST, page);
        rpc.flushIndexQueue();
        return product.getPageBinder().bind(LiveSearchMacro.class);
    }

    @Test
    public void testPressingButtonPerformsGlobalSearch()
    {
        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch}");
        TestContext context = TestContext.builder().setRpc(rpc).addPages(liveSearchPage).build();

        context.create();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        SearchPage searchPage = macro.clickSearch();
        assertNotNull(searchPage);

        context.delete();
    }

    //CONFDEV-19661
    @Test
    public void testDropDownShouldAppearNoResultRecordWhenSearchLengthEqual3()
    {
        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch}");
        TestContext context = TestContext.builder().setRpc(rpc).addPages(liveSearchPage).build();

        context.create();

        rpc.flushIndexQueue();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        String searchText = "aaa";
        Iterator<LiveSearchSearchResult> resultItr = macro.searchFor(searchText).iterator();
        assertTrue(resultItr.hasNext());
        LiveSearchSearchResult result = resultItr.next();
        assertEquals("No results found for '" + searchText + "'", result.getTitle());
        assertFalse(resultItr.hasNext());

        context.delete();
    }

    // CONFDEV-19661
    @Test
    public void testDropDownShouldNotAppearWhenSearchLengthEqual2()
    {
        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch}");
        TestContext context = TestContext.builder().setRpc(rpc).addPages(liveSearchPage).build();

        context.create();

        rpc.flushIndexQueue();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        String searchText = "aa";
        Iterator<LiveSearchSearchResult> resultItr = macro.searchFor(searchText).iterator();
        assertFalse(resultItr.hasNext());
        assertFalse(macro.isDropDownVisible());

        context.delete();
    }

    @Test
    public void testPressingButtonPerformsSpaceSpecificSearchWhenSpaceKeyParamSpecified()
    {

        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch:spaceKey=TST}");
        Page pageToNNotFind = new Page(Space.TEST2, "Not Find", "test");

        TestContext context = TestContext.builder().setRpc(rpc)
                .addSpaces(Space.TEST2).addPages(liveSearchPage, pageToNNotFind).build();

        context.create();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        macro.searchFor("test");

        SearchPage searchPage = macro.clickSearch();
        List<PageElement> results = searchPage.results();

        for (PageElement result : results)
        {
            assertThat(result.getText(), not(containsString(pageToNNotFind.getTitle())));
        }

        context.delete();
    }

    @Test
    public void testLiveSearchWithSpaceScope()
    {
        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch:spaceKey=TST}");

        TestContext context = TestContext.builder().setRpc(rpc).addPages(liveSearchPage).build();
        context.create();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        Iterable<LiveSearchSearchResult> results = macro.searchFor("test");

        Predicate<LiveSearchSearchResult> additionalPredicate = searchResultByAdditional(Space.TEST.getName());
        Predicate<LiveSearchSearchResult> contentTypePredicate = searchResultByContentType("spacedesc");
        Predicate<LiveSearchSearchResult> spaceUrlpredicate = searchResultByUrl(Space.TEST.getUrlPath());

        // non space
        Iterable<LiveSearchSearchResult> nonSpaceFiltered = Iterables.filter(results,
                Predicates.and(additionalPredicate, Predicates.not(contentTypePredicate)));

        // space
        Iterable<LiveSearchSearchResult> spaceFiltered = Iterables.filter(results,
                Predicates.and(spaceUrlpredicate, contentTypePredicate));

        List<LiveSearchSearchResult> resultList = Lists.newArrayList(results);
        List<LiveSearchSearchResult> nonSpaceList = Lists.newArrayList(nonSpaceFiltered);
        List<LiveSearchSearchResult> spaceList = Lists.newArrayList(spaceFiltered);

        assertEquals(resultList.size(), nonSpaceList.size() + spaceList.size());

        context.delete();
    }

    // CONF-7496
    @Test
    public void testStemming()
    {
        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch:spaceKey=TST}");
        Page eclipsePage = new Page(Space.TEST, "page title 1", "eclipse");
        Page eclipseAndStuffPage = new Page(Space.TEST, "page title 2", "eclipse and stuff");

        TestContext context = TestContext.builder().setRpc(rpc)
                .addPages(eclipsePage, eclipseAndStuffPage, liveSearchPage).build();

        context.create();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        Iterable<LiveSearchSearchResult> results = macro.searchFor("eclipse* OR eclipse");

        Predicate<LiveSearchSearchResult> resultsByEclipseTitle = searchResultByTitle(eclipsePage.getTitle());
        Predicate<LiveSearchSearchResult> resultsByEclipseAndStuffTitle =
                searchResultByTitle(eclipseAndStuffPage.getTitle());


        Iterable<LiveSearchSearchResult> filtered = Iterables.filter(results,
                Predicates.or(resultsByEclipseTitle, resultsByEclipseAndStuffTitle));

        List<LiveSearchSearchResult> resultList = Lists.newArrayList(results);
        List<LiveSearchSearchResult> filteredList = Lists.newArrayList(filtered);

        assertEquals(resultList.size(), 2);
        assertEquals(filteredList.size(), 2);

        context.delete();
    }

}
