package it.com.atlassian.confluence.extra.livesearch;

import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import com.atlassian.maven.plugins.unpacktests.ExportedTest;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import it.com.atlassian.confluence.plugins.livesearch.TestContext;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchMacro;
import it.com.atlassian.confluence.plugins.livesearch.pageobjects.LiveSearchSearchResult;
import org.junit.After;
import org.junit.Test;

import static it.com.atlassian.confluence.plugins.livesearch.LiveSearchMacroTestUtil.*;
import static org.junit.Assert.assertEquals;

@ExportedTest
public class LiveSearchExportedTest extends AbstractWebDriverTest
{
    @After
    public void tearDown()
    {
        rpc.removeSpace(Space.TEST.getKey());
    }

    private LiveSearchMacro getLiveSearchMacro(Page page)
    {
        rpc.logIn(User.CONF_ADMIN);
        product.loginAndView(User.TEST, page);
        rpc.flushIndexQueue();
        return product.getPageBinder().bind(LiveSearchMacro.class);
    }

    @Test
    public void testLiveSearchWithGlobalScope()
    {
        Page liveSearchPage = new Page(Space.TEST, "Livesearch Test", "{livesearch}");

        TestContext context = TestContext.builder().setRpc(rpc).addPages(liveSearchPage).build();
        context.create();

        LiveSearchMacro macro = getLiveSearchMacro(liveSearchPage);

        Iterable<LiveSearchSearchResult> results = macro.searchFor("test");

        //contains test page
        Predicate<LiveSearchSearchResult> predicate = searchResultByUrl(Page.TEST.getUrl());
        Iterable<LiveSearchSearchResult> filtered = Iterables.filter(results, predicate);
        assertEquals(Lists.newArrayList(filtered).size(), 1);

        //contains test space
        Predicate<LiveSearchSearchResult> spaceUrlpredicate = searchResultByUrl(Space.TEST.getUrlPath());
        Predicate<LiveSearchSearchResult> contentTypePredicate = searchResultByContentType("spacedesc");
        filtered = Iterables.filter(results, Predicates.and(spaceUrlpredicate, contentTypePredicate));
        assertEquals(Lists.newArrayList(filtered).size(), 1);

        context.delete();
    }
}
